// import Home from "../components/Home";
// import Layout from "../components/Layout";
// import styles from "../styles/Home.module.css";
import Head from "next/head";
import Header from "../components/Header";
import Sidebar from "../components/Sidebar";
import Feed from "../components/Feed";
import Widgets from "../components/Widgets";

const posts = [
  {
    name: "Post 1",
  },
  {
    name: "Post 2",
  },
  {
    name: "Post 3",
  },
];
const HomePage = () => (
  // <Layout>
  //   <Home />
  // </Layout>
  <div className="h-screen bg-gray-100 overflow-hidden">
    <Head>
      <title>facebook</title>
    </Head>

    <Header />

    <main className="flex">
      {/* Sidebar */}
      <Sidebar />
      {/* Feed */}
      <Feed posts={posts} />
      {/* Widgets */}
      <Widgets />
    </main>
  </div>
);

export default HomePage;
