// next.config.js
module.exports = {
  images: {
    domains: [
      "restcountries.eu",
      "links.papareact.com",
      "platform-lookaside.fbsbx.com",
      "firebasestorage.googleapis.com",
      "fakestoreapi.com",
      "www.google.co.uk",
      "i.ibb.co",
    ],
  },
};
